<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class todo_controller extends base_controller {
    protected $config = array('app_type' => 'personal');
    //过滤查询字段
    function _search_filter(&$map) {
        $map[] = array('name', 'like', request('keyword'));
    }

    public function index() {
        $user_id = get_user_id();
        $where[] = array('user_id', 'eq', $user_id);
        $where[] = array('status', 'in', array(1, 2));
        $keyword = request('keyword');
        if (!empty($keyword)) {
            $where[] = array('name', 'like', post('keyword'));
        }
        $list = model("todo") -> where($where) -> order('priority desc,sort asc') -> get_list();
        $this -> assign("list", $list);

        $where2[] = array('user_id', 'eq', $user_id);
        $where2[] = array('status', 'eq', 3);
        $list2 = model("todo") -> where($where2) -> order('priority desc,sort asc') -> get_list();
        $this -> assign("list2", $list2);

        $this -> display();
    }

    public function upload() {
        $this -> _upload();
    }

    function read($id) {
        $model = model('Todo');
        $list = $_REQUEST['list'];
        $this -> assign("list", $list);
        $list = array_filter(explode("|", $list));
        array_pop($list);
        $current = array_search($id, $list);
        if ($current !== false) {
            $next = $list[$current + 1];
            $prev = $list[$current - 1];
        }
        $this -> assign('next', $next);
        $this -> assign('prev', $prev);

        $where['id'] = $id;
        $where['user_id'] = get_user_id();
        $vo = $model -> where($where) -> find();
        $this -> assign('vo', $vo);
        $this -> display();
    }

    public function down($attach_id) {
        $this -> _down($attach_id);
    }

    function del() {
        $id = request('id');
        $where[] = array('id','eq',$id);
        $where[] = array('user_id','eq',get_user_id());
        $result = model("Todo") -> where($where) -> delete();
        if ($result !== false) {//保存成功
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('删除成功!');
        } else {
            //失败提示
            $this -> error('删除失败!');
        }
    }

    public function add() {
        $plugin['jquery-ui'] = true;
        $plugin['date'] = true;
        $plugin['uploader'] = true;
        $plugin['editor'] = true;
        $this -> assign("plugin", $plugin);
        $this -> display();
    }

    public function edit($id) {
        $plugin['jquery-ui'] = true;
        $plugin['date'] = true;
        $plugin['uploader'] = true;
        $plugin['editor'] = true;
        $this -> assign("plugin", $plugin);

        $this -> assign("time_list", $time_list);
        $model = model('Todo');
        $where['user_id'] = get_user_id();
        $where['id'] = $id;
        $vo = $model -> where($where) -> find();

        $vo['start_time'] = fix_time($vo['start_time']);
        $vo['end_time'] = fix_time($vo['end_time']);
        $this -> assign('vo', $vo);
        $this -> display();
    }

    public function set_sort() {
        $node = $_REQUEST['node'];
        $priority = $_REQUEST['priority'];
        $sort = $_REQUEST['sort'];

        $model = model("Todo");
        // 实例化User对象
        $where['user_id'] = get_user_id();
        foreach ($node as $key => $val) {
            $data['sort']=$sort[$key];
            $data['id']=$val;            
            $model -> save($data);
        }
    }

    public function mark_status() {
        $id = request('id');
        $val = request('val');
        if ($val == 3) {
            $data['end_date'] = date("Y-m-d");
            $data['id']=$id;
            $result=model('todo')->save($data);
        }
        $data['status'] = $val;
        $data['id']=$id;
        $result=model('todo')->save($data);
                 
        if ($result !== false) {//保存成功
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('操作成功!');
        } else {
            //失败提示
            $this -> error('操作失败!');
        }
    }

    function json() {
        header("Cache-Control: no-cache, must-revalidate");
        header("Content-Type:text/html; charset=utf-8");
        $user_id = get_user_id();
        $start_date = $_REQUEST["start_date"];
        $end_date = $_REQUEST["end_date"];

        $where['user_id'] = $user_id;
        $where['start_date'] = array( array('gt', $start_date), array('lt', $end_date));
        $list = model("Todo") -> where($where) -> order('start_date,priority desc') -> select();
        exit(json_encode($list));
    }

}
?>