<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class customer_controller extends base_controller {
	protected $config = array('app_type' => 'common');

	//过滤查询字段
	function _search_filter(&$map) {
		$map[] = array('is_del', 'eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$where[] = array('name', 'like', $keyword, 'or');
			$where[] = array('letter', 'like', $keyword, 'or');
			$where[] = array('contact', 'like', $keyword, 'or');
			$where[] = array('email', 'like', $keyword, 'or');
			$where[] = array('office_tel', 'like', $keyword, 'or');
			$where[] = array('mobile_tel', 'like', $keyword, 'or');
			$where[] = array('salesman', 'like', $keyword, 'or');
			$map[] = array('complex', $where);
		}
	}

	function index() {
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
		$model = model("customer");
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> display();
	}

	function export() {
		$model = model("customer");		
		$where[] = array('is_del', 'eq', 0);
		$list = $model -> where($where) -> get_list();
		vendor();

		$objPHPExcel = new \PHPExcel();
		$i = 1;
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", "全称");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("B$i", "简称");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("C$i", "证件号码");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("D$i", "付款方式");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("E$i", "地址");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("F$i", "业务员");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("G$i", "联系人");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("H$i", "邮箱");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("I$i", "办公电话");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("I$i", "手机");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("J$i", "传真");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("K$i", "即时聊天");
		$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("K$i", "其他");

		//dump($list);
		foreach ($list as $val) {
			$i++;
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("A$i", $val["name"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("B$i", $val["short"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("C$i", $val["biz_license"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("D$i", $val["payment"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("E$i", $val["address"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("F$i", $val["salesman"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("G$i", $val["contact"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("H$i", $val["email"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("I$i", $val["office_tel"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("J$i", $val["mobile_tel"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("J$i", $val["fax"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("J$i", $val["im"]);
			$objPHPExcel -> setActiveSheetIndex(0) -> setCellValue("K$i", $val["remark"]);
		}
		// Rename worksheet
		$objPHPExcel -> getActiveSheet() -> setTitle('customer');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel -> setActiveSheetIndex(0);
		$file_name = "customer.xlsx";
		// Redirect output to a client’s web browser (Excel2007)
		header("Content-Type: application/force-download");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition:attachment;filename =" . str_ireplace('+', '%20', URLEncode($file_name)));
		header('Cache-Control: max-age=0');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter -> save('php://output');
		exit ;
	}

	public function import() {
		$opmode = request('opmode');
		if ($opmode == "import") {
			$file = model('file');
			$info = $file -> upload($_FILES['uploadfile']);
			if (!$info) {
				$this -> error($File -> getError());
			} else {
				//取得成功上传的文件信息
				vendor();
				//导入thinkphp第三方类库
				$upload_path = config('upload_path');
				$upload_path = 'uploads';

				$file_path = $upload_path . DS . $info["save_path"] . DS . $info["save_name"];

				$objPHPExcel = \PHPExcel_IOFactory::load($file_path);
				$sheetData = $objPHPExcel -> getActiveSheet() -> toArray(null, true, true, true);
				$model = model("customer");

				for ($i = 2; $i <= count($sheetData); $i++) {
					$data = array();
					$data['name'] = mb_substr($sheetData[$i]["A"], 0, 50);
					$data['company'] = mb_substr($sheetData[$i]["B"], 0, 50);
					$data['letter'] = mb_substr(get_letter($sheetData[$i]["A"]), 0, 50);
					$data['dept'] = mb_substr($sheetData[$i]["C"], 0, 50);
					$data['position'] = mb_substr($sheetData[$i]["D"], 0, 50);
					$data['email'] = mb_substr($sheetData[$i]["G"], 0, 50);
					$data['office_tel'] = mb_substr($sheetData[$i]["E"], 0, 50);
					$data['mobile_tel'] = mb_substr($sheetData[$i]["F"], 0, 50);
					$data['website'] = mb_substr($sheetData[$i]["I"], 0, 50);
					$data['im'] = mb_substr($sheetData[$i]["H"], 0, 50);
					$data['address'] = mb_substr($sheetData[$i]["J"], 0, 50);
					$data['user_id'] = get_user_id();
					$data['remark'] = mb_substr($sheetData[$i]["K"], 0, 50);
					$data['is_del'] = 0;
					$model -> add($data);
				}
				//dump($sheetData);
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$this -> assign('jumpUrl', get_return_url());
				$this -> success('导入成功！');
			}
		} else {
			$this -> display();
		}
	}

	function del($id) {
		$this -> _del($id);
	}

	protected function _insert($name = 'customer') {
		$model = model('customer');
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> letter = get_letter($model -> name);
		//保存当前数据对象
		$list = $model -> add();
		if ($list !== false) {//保存成功
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('新增成功!');
		} else {
			//失败提示
			$this -> error('新增失败!');
		}
	}

	protected function _update($name = 'customer') {
		$id = $_POST['id'];
		$model = model("customer");
		if (false === $model -> create()) {
			$this -> error($model -> getError());
		}
		$model -> letter = get_letter($model -> name);
		// 更新数据
		$list = $model -> save();
		if (false !== $list) {
			//成功提示
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('编辑成功!');
		} else {
			//错误提示
			$this -> error('编辑失败!');
		}
	}

}
?>