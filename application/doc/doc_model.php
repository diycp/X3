<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  doc_model extends base_model {
	// 自动验证设置
	protected $_validate = array( array('name', 'require', '文件名必须', 1), array('content', 'require', '内容必须'), );
	protected $filter_config = array('skip' => 'acbc,abcd,defb', 'filter_abc' => 'name,sdfed,bcde,asdgt');

	function filter_abc($val) {
		if ($val == 'abc') {
			return 'abcabc';
		}
		return $val;
	}

	protected function _after_add(&$data) {
		$id = $data['id'];

		$where_cout[] = array('id', 'elt', $id);
		$where_cout[] = array('create_time', 'egt', mktime(0, 0, 0, 1, 1, date('Y')));

		$model = model('doc');
		$count = $model -> where($where_cout) -> count();
		$doc_no = date('Y') . '-' . str_pad($count, 4, '0', STR_PAD_LEFT);

		$where[] = array('id', 'eq', $id);
		$model -> where($where) -> set_field('doc_no', $doc_no);
	}

	function test($return_data = false) {
		$this -> select('doc.*,system_folder.name folder_name');
		$this -> from('doc');
		$this -> inner('system_folder', 'doc.folder=system_folder.id and system_folder.controller=\'' . APP_NAME . '\'');
		if ($return_data) {
			return $this -> get_list();
		} else {
			return $this;
		}
	}

}
?>