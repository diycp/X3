<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class udf_field_controller extends base_controller {
	protected $config = array('app_type' => 'asst');

	function add() {
		$controller = request('controller');
		$this -> assign('controller', $controller);
		$row_type = request('row_type');
		$this -> assign('row_type', $row_type);
		$this -> display();
	}

	function index() {
		$row_type = request('row_type');
		if (IS_POST) {
			$opmode = request("opmode");
			$model = model("udf_field");
			if (false === $model -> create()) {
				$this -> error($model -> getError());
			}
			if ($opmode == "add") {
				$list = $model -> add();
				if ($list !== false) {//保存成功
					$this -> assign('jumpUrl', get_return_url());
					$this -> success('新增成功!');
				} else {
					$this -> error('新增失败!');
					//失败提示
				}
			}
			if ($opmode == "edit") {
				$list = $model -> save();
				if ($list !== false) {//保存成功
					$this -> assign('jumpUrl', get_return_url());
					$this -> success('保存成功!');
				} else {
					$this -> error('保存失败!');
					//失败提示
				}
			}
			if ($opmode == "del") {
				$id = request('id');
				$where[]=array('id','eq',$id);
				$list = $model -> where($where) -> delete();
				if ($list !== false) {//保存成功
					$this -> assign('jumpUrl', get_return_url());
					$this -> success('删除成功!');
				} else {
					$this -> error('删除失败!');
					//失败提示
				}
			}
		}

		$controller = APP_NAME;
		$this -> assign('controller', APP_NAME);
		$model = model("udf_field");

		$this -> assign('row_type', $row_type);
		$this -> assign('folder_name',$this->folder_name);

		$where[] = array('row_type', 'eq', $row_type);
		$where[] = array('is_del', 'eq', 0);
		$where[] = array('controller', 'eq', $controller);

		$field_list = $model -> where($where) -> order('sort asc') -> get_list();

        $tree = list_to_tree($field_list);
        $this -> assign('tree_menu', $tree);
		

		$this -> assign("field_list", $field_list);
		$this -> display('udf_field/view/index.html');
	}

	public function import_data() {
		$opmode = post('opmode');
		
		$row_type = request('row_type');
		$this -> assign('row_type', $row_type);
			
		$controller = request('controller');
		$this -> assign('controller', $controller);
				
		if ($opmode == "import") {
			$row_type = request('row_type');
			
			$file = model('file');
			$info = $file -> upload($_FILES['uploadfile']);
			
			if (!$info) {
				$this -> error('上传错误');
			} else {
				//取得成功上传的文件信息
				vendor();
				$root_path = config('root_path');
				$root_path = 'uploads';
				$file_path = $root_path . DS . $info['save_path'] . DS . $info['save_name'];

				$excel = \PHPExcel_IOFactory::load($file_path);

				$sheet_data = $excel -> getActiveSheet() -> toArray(null, true, true, true);
				$model = model("udf_field");
				for ($i = 1; $i <= count($sheet_data); $i++) {
					$data = array();
					$data['row_type'] = $row_type;
					$data['name'] = $sheet_data[$i]["A"];
					$data['sort'] = $sheet_data[$i]["B"];
					$data['msg'] = $sheet_data[$i]["C"];
					$data['type'] = $sheet_data[$i]["D"];
					$data['layout'] = $sheet_data[$i]["E"];
					$data['data'] = $sheet_data[$i]["F"];
					$data['validate'] = $sheet_data[$i]["G"];
					$data['config'] = $sheet_data[$i]["H"];
					$data['controller'] = $controller;
					$user_id = $model -> add($data);
				}
				//dump($sheet_data);
				$return['status'] = 1;
				$return['info'] = '导入成功';
				$this -> assign('jumpUrl', get_return_url());
				$this -> success('导入成功');
			}
		} else {
			$tpl_name = 'contact.xlsx';
			$this -> assign('tpl_name', $tpl_name);
			$this -> display();
		}
	}

	function export_data($row_type) {

		$model = model('udf_field');
		$where[] = array('row_type','eq', $row_type);
		$list = $model -> where($where) -> get_list();

		vendor();

		$excel = new \PHPExcel();		
		// Add some data
		$i = 0;

		foreach ($list as $val) {
			$i++;
			$excel -> setActiveSheetIndex(0) -> setCellValue("A$i", $val["name"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("B$i", $val["sort"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("C$i", $val["msg"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("D$i", $val["type"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("E$i", $val["layout"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("F$i", $val["data"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("G$i", $val["validate"]);
			$excel -> setActiveSheetIndex(0) -> setCellValue("H$i", $val["config"]);
		}
		// Rename worksheet
		$excel -> getActiveSheet() -> setTitle('user defiend field');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel -> setActiveSheetIndex(0);
		$file_name = "udf_field.xlsx";
		// Redirect output to a client’s web browser (Excel2007)
		header("Content-Type: application/force-download");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition:attachment;filename =" . str_ireplace('+', '%20', URLEncode($file_name)));
		header('Cache-Control: max-age=0');

		$objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		//readfile($filename);
		$objWriter -> save('php://output');
		exit ;
	}

}
