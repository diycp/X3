<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  udf_field_model extends base_model {

	public function get_field_list($row_type, $controller = APP_NAME) {
		$where[] = array('row_type', 'eq', $row_type);
		$where[] = array('controller', 'eq', $controller);
		$where[] = array('is_del', 'eq', '0');

		$list = $this -> where($where) -> order('sort asc') -> get_list();
		return $list;
	}

	public function get_show_field($udf_data) {
		$field_data = json_decode($udf_data, true);
		$field_id = array_keys($field_data);

		$where_field['id'] = array('in', $field_id);
		$list_field = $this -> where($where_field) -> select();

		foreach ($list_field as $key => $field) {
			if (strpos($field['config'], 'show') !== false) {
				$return[] = $field;
			}
		}
		return $return;
	}

	public function get_data_list($row_type, $udf_data = null, $controller = APP_NAME) {
		$where_field[] = array('row_type', 'eq', $row_type);
		$where_field[] = array('controller', 'eq', $controller);
		$where_field[] = array('is_del', 'eq', 0);
		$field_list = $this -> where($where_field) -> order('sort asc') -> get_list();

		if (!empty($udf_data)) {
			$udf_data = json_decode($udf_data, true);
			foreach ($field_list as $key => $field) {
				$val[$key] = $field;
				if (isset($udf_data[$field['id']])) {
					$val[$key]['val'] = $udf_data[$field['id']];
				}
			}
			return $val;
		} else {
			return $field_list;
		}
	}

	public function get_field_name($udf_name) {
		if (!empty($udf_name)) {
			$field_data = json_decode($udf_name, true);
			$field_id = array_keys($field_data);

			$where_field['id'] = array('in', $field_id);
			$list_field = $this -> where($where_field) -> select();

			foreach ($list_field as $key => $field) {
				$val[$key] = $field["name"];

			}

			return $val;
		}
	}

	function get_field_data() {
		$udf_field = array_filter(array_keys($_REQUEST), array($this, 'filter_udf_field'));
		if (!empty($udf_field)) {
			foreach ($udf_field as $field) {
				$tmp = array_filter(explode("_", $field));
				$val = $_REQUEST[$field];

				if (is_array($val)) {
					$val = implode("|", $val);
				}
				$field_data[$tmp[2]] = $val;
			}
			return json_encode($field_data, JSON_UNESCAPED_UNICODE);
		} else {
			return;
		}
	}

	function filter_udf_field($val) {
		if (strpos($val, "udf_field") !== false) {
			return true;
		} else {
			return false;
		}
	}

}
?>