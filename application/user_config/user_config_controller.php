<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/sef
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class user_config_controller extends base_controller {

    public function index() {
        $vo = model('user_config') -> find(get_user_id());
        $this -> assign('vo', $vo);
        $this -> display();
    }

    function save() {
        $config = model("user_config") -> find(get_user_id());
        if (count($config)) {
            $this -> _update();
        } else {
            $this -> _insert();
        }
    }

    function _insert($name = 'user_config') {
        $model = model('user_config');
        if (false === $model -> create()) {
            $this -> error($model -> getError());
        }

        //保存当前数据对象
        $list = $model -> add();
        if ($list !== false) {//保存成功
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('新增成功!');
        } else {
            //失败提示
            $this -> error('新增失败!');
        }
    }

    function _update($name = 'user_config') {
        //B('FilterString');
        $model = model('user_config');
        if (false === $model -> create()) {
            $this -> error($model -> getError());
        }
        $model -> id = get_user_id();
        // 更新数据
        $list = $model -> save();
        if (false !== $list) {
            //成功提示
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('编辑成功!');
        } else {
            //错误提示
            $this -> error('编辑失败!');
        }
    }

}
?>