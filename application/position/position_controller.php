<?php

use sef\controller;
use sef\model;

class position_controller extends base_controller {
	protected $config = array('app_type' => 'master');

	function _search_filter(&$map) {
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$map[] = array('name', 'like', $keyword);
		}
	}

	/**列表页面 **/
	function index() {
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}
		$model = model('position');
		if (!empty($model)) {
			$this -> _list($model, $map, 'sort');
		}
		$this -> display();
	}

	function del($id) {
		$this -> _destory($id);
	}

}
?>