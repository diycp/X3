<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class MeetingController extends HomeController {
	protected $config = array('app_type' => 'common', 'admin' => 'roomindex,addroom,delroom,saveroom,readroom', 'read' => 'json,day_view');
	//过滤查询字段
	function _search_filter(&$map) {
		if (!empty($_POST["name"])) {
			$map['name'] = array('like', "%" . $_POST['name'] . "%");
		}
		$map['user_id'] = array('eq', get_user_id());
		$map['is_del'] = array('eq', '0');
	}

	public function upload() {
		$this -> _upload();
	}

	function index() {
		$auth = $this -> auth;
		$this -> assign('auth', $auth);

		$plugin['calendar'] = true;
		$this -> assign("plugin", $plugin);
		$this -> display();
	}

	function read($id) {
		$plugin['jquery-ui'] = true;
		$this -> assign("plugin", $plugin);
		$list = M("MeetingRoom") -> where('is_del=0') -> order('sort asc') -> getField('id,name');
		$this -> assign('room_list', $list);

		$model = M('Meeting');
		$list = $_REQUEST['list'];
		$this -> assign("list", $list);
		$list = array_filter(explode("|", $list));
		$current = array_search($id, $list);

		if ($current !== false) {
			$next = $list[$current + 1];
			$prev = $list[$current - 1];
		}
		$this -> assign('next', $next);
		$this -> assign('prev', $prev);

		$where['id'] = $id;
		// $where['user_id'] = get_user_id();
		$vo = $model -> where($where) -> find();
		$vo['meeting_room'] = M('MeetingRoom') -> where(array('id' => $vo['meeting_room'])) -> getField('name');
		$this -> assign('vo', $vo);
		$this -> display();
	}

	public function down($attach_id) {
		$this -> _down($attach_id);
	}

	public function add() {

		$plugin['jquery-ui'] = true;
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
		$list = M("MeetingRoom") -> where('is_del=0') -> order('sort asc') -> getField('id,name,num,addr,projector,media');
		$this -> assign('room_list', $list);

		$this -> display();
	}

	public function edit($id) {
		$plugin['jquery-ui'] = true;
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);
		$list = M("MeetingRoom") -> where('is_del=0') -> order('sort asc') -> getField('id,name,num,addr,projector,media');
		$this -> assign('room_list', $list);

		$id = request('id');
		$model = M('Meeting');
		// $where['user_id'] = get_user_id();
		$where['id'] = $id;
		$vo = $model -> where($where) -> find();

		$this -> assign('vo', $vo);
		$this -> display();
	}

	public function day_view() {
		$this -> index();
	}

	public function del($id) {
		$where['meeting_id'] = array('eq', $id);

		$model = M("MeetingScope");
		$result = $model -> where($where) -> delete();
		$this -> _del($id);
	}

	function json() {
		header("Cache-Control: no-cache, must-revalidate");
		header("Content-Type:text/html; charset=utf-8");

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$sourse = $_REQUEST["sourse"];
		$user_id = get_user_id();
		switch ($sourse) {
			case 'created' :
				$where['user_id'] = $user_id;
				break;

			case 'actor' :
				$where_scope['user_id'] = $user_id;
				$meeting_list = M("MeetingScope") -> where($where_scope) -> getField('meeting_id', true);
				if (!empty($meeting_list)) {
					// $meeting_list = implode(",", $meeting_list);
					$where['id'] = array('in', $meeting_list);
				} else {
					$where['_string'] = '1=2';
				}
				break;

			case 'all' :
				break;

			default :
				$where['_string'] = '1=2';
				break;
		}
		$where['is_del'] = array('eq', 0);
		$where['start_time'] = array( array('egt', $start_date), array('elt', $end_date));
		$list = M("Meeting") -> where($where) -> order('start_time') -> select();
		$meeting_room = M('MeetingRoom') -> getField('id,name');
	 
		foreach ($list as $key => $value) {
			$list[$key]['meeting_room'] = $meeting_room[$value['meeting_room']];
		}
		exit(json_encode($list));
	}

	function roomindex() {
		$model = M("MeetingRoom");
		$list = $model -> order('sort asc') -> getField('id,name,is_del');
		$this -> assign('list', $list);
		$this -> display();
	}

	public function readroom($id) {
		$this -> _edit($id, "MeetingRoom");

	}

	public function delroom($id) {
		$this -> _destory($id, 'MeetingRoom');
	}

	public function saveroom() {
		$this -> _save('MeetingRoom');
	}

}
?>