<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class kf_favorite_controller extends base_controller {
	protected $auth_map = array('admin' => 'test1,test,del,move_to', 'read' => 'index,folder_manage');

    function _search_filter(&$map) {
        $map[] = array('user_id', 'eq', get_user_id());        
        $keyword = request('keyword');
        if (!empty($keyword)) {
            $where[] = array('name', 'like', $keyword, 'or');
            $where[] = array('company', 'like', $keyword, 'or');
            $where[] = array('dept', 'like', $keyword, 'or');
            $where[] = array('position', 'like', $keyword, 'or');
            $where[] = array('office_tel', 'like', $keyword, 'or');
            $where[] = array('mobile_tel', 'like', $keyword, 'or');
            $where[] = array('letter', 'like', $keyword, 'or');
            $map[] = array('complex', $where);
        }
    }
	
	function del($id) {
		$this -> _destory($id);
	}
}
