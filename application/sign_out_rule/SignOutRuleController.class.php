<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class SignOutRuleController extends HomeController {
	protected $config = array('app_type' => 'public');
	//过滤查询字段
	function _search_filter(&$map) {
		if (!empty($_POST["keyword"])) {
			$map['name'] = array('like', "%" . $_POST['keyword'] . "%");
		}
		$map['is_real_time'] = array('eq', 0);
	}

	public function index() {
		$flow_type = M('FlowType') -> getFIeld('id,name');
		$this -> assign('flow_type', $flow_type);
		$this -> _index();
	}

	public function add() {

		$where['is_del'] = 0;
		$flow_type_list = M('FlowType') -> where($where) -> order('sort') -> getField('id,name');

		$this -> assign('flow_type_list', $flow_type_list);
		$this -> display();
	}

	public function edit($id) {
		$where['is_del'] = 0;
		$flow_type_list = M('FlowType') -> where($where) -> order('sort') -> getField('id,name');

		$this -> assign('flow_type_list', $flow_type_list);

		$vo = M('sign_out_rule') -> find($id);
		$this -> assign('vo', $vo);
		$this -> display();
	}

	public function del() {
		$id = $_POST['id'];
		$sign_rule = M('sign_out_rule');
		$rs = $sign_rule -> where("id = '$id'") -> delete();
		if ($rs) {
			$this -> success('删除成功', U('sign_rule'));
		}
	}

	function udf_field() {
		$flow_type = request('flow_type');

		$where['row_type'] = array('eq', $flow_type);
		$udf_field = M('UdfField') -> where($where) -> field('id,name') -> order('sort asc') -> select();

		$tree = list_to_tree($udf_field);
		$this -> assign('menu', popup_tree_menu($tree));
		$this -> display();
	}

	public function import_flow() {
		if (IS_POST) {
			$out_rule = request('out_rule');
			if (!empty($out_urle)) {
				$where['id'] = array('eq', $out_rule);
			} else {
				$where['_string'] = '1=1';
			}
			$list = M('SignOutRule') -> where($where) -> select();

			$start_date = request('start_date');
			$end_date = request('end_date');
			$success = 0;
			$error = 0;
			foreach ($list as $val) {
				//删除数据
				$where_del['sign_name'] = $val['name'];
				$where_del['sign_date'] = array( array('egt', $start_date), array('elt', $end_date));
				$where_del['type'] = 'sign_import';
				M('Sign') -> where($where_del) -> delete();

				$where_flow['type'] = array('eq', $val['flow_type']);
				$where_flow['create_time'] = array('egt', date_to_int($start_date), 'elt', date_to_int($end_date) + 86400);
				$where_flow['step'] = array('eq', 40);
				$flow_list = M('Flow') -> where($where_flow) -> select();
				foreach ($flow_list as $flow) {
					$udf_data = json_decode($flow['udf_data'], true);
					$data['user_id'] = $flow['user_id'];
					$data['user_name'] = $flow['user_id'];
					$data['emp_no'] = $flow['emp_no'];
					$data['sign_name'] = $val['name'];
					$data['type'] = 'sign_import';
					$data['status'] = 1;
					$data['is_real_time'] = 0;
					$data['content'] = get_flow_type_name($flow['type']) . " : " . $flow['doc_no'];

					$start_date_field = $val['start_date_field_id'];
					$sign_start_date = substr($udf_data[$start_date_field], 0, 10);

					$end_date_field = $val['end_date_field_id'];
					$sign_end_date = substr($udf_data[$end_date_field], 0, 10);

					if (is_date($sign_start_date, 'Y-m-d') && is_date($sign_end_date, 'Y-m-d')) {
						$diff = abs(strtotime($sign_start_date) - strtotime($sign_end_date)) / 86400;
						for ($i = 0; $i < $diff + 1; $i++) {
							$Y = date('Y', strtotime($sign_start_date));
							$m = date('m', strtotime($sign_start_date));
							$d = date('d', strtotime($sign_start_date));

							$data['sign_date'] = date("Y-m-d", mktime(0, 0, 0, $m, $d + $i, $Y));
							$data['create_time'] = mktime(0, 0, 1, $m, $d + $i, $Y);
							M('Sign') -> add($data);
							$success++;
						}
					} else {
						$error++;
						$error_msg .= $flow['doc_no'] . ',';
					};
				}
			}
			$return['info'] = "成功导入{$success}条;{$error}条发生错误;编号:{$error_msg}";
			$return['status'] = 1;
			$this -> ajaxReturn($return);
		} else {
			$plugin['date'] = true;
			$this -> assign("plugin", $plugin);

			$out_rule = M('SignOutRule') -> getField('id,name');
			$this -> assign('out_rule', $out_rule);
			$this -> display();
		}
	}

}
?>