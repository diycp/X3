<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use framework\controller;
use framework\model;

class html_controller extends base_controller {
    protected $app_type = 'widget';
    protected $is_widget = true;

    function sidebar($tree, $level = 0) {
        $level++;
        $html = "";
        if (is_array($tree)) {
            if ($level > 1) {
                $id = $tree[0]['pid'];
                $html .= "<input type=\"checkbox\" id=\"chk_$id\" checked=\"true\">";
                $html .= "<label for=\"chk_$id\"><i class=\"fa fa-angle-down\"></i></label><ul>\r\n";
            } else {
                $html = "<ul>\r\n";
            }

            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    if (!empty($val["url"])) {
                        if (strpos($val['url'], "##") !== false) {
                            $url = "#";
                        } else if (strpos($val['url'], 'http') !== false) {
                            $url = $val['url'];
                        } else {
                            $url = url($val['url']);
                        }
                    } else {
                        $url = "#";
                    }
                    if (empty($val["id"])) {
                        $id = $val["name"];
                    } else {
                        $id = $val["id"];
                    }
                    if (isset($val['_child'])) {
                        $html .= "<li>\r\n";
                        $html .= "<a node=\"$id\" url=\"" . "$url\">$title<i class='count'></i></a>\r\n";
                        $html .= $this -> sidebar($val['_child'], $level);
                        $html = $html . "</li>\r\n";
                    } else {
                        $html .= "<li>\r\n";
                        $html .= "<a node=\"$id\" url=\"" . "$url\">$title<i class='count'></i></a>\r\n";
                        $html = $html . "</li>\r\n";
                    }
                }
            }
            $html = $html . "</ul>\r\n";
        }
        return $html;
    }

    function tree_menu($tree, $level = 0) {
        $level++;
        $html = "";
        if (is_array($tree)) {
            if ($level > 1) {
                $id = $tree[0]['pid'];
                $html .= "<input type=\"checkbox\" id=\"chk_tree_$id\" checked=\"true\">";
                $html .= "<label for=\"chk_tree_$id\"><i class=\"fa fa-plus-square-o\"></i></label><ul>\r\n";
            } else {
                $html = "<ul>\r\n";
            }            
            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    $id = $val["id"];
                    if (isset($val['_child'])) {
                        $html = $html . "<li>\r\n<a node=\"$id\" >$title</a>\r\n";
                        $html = $html . $this->tree_menu($val['_child'], $level);
                        $html = $html . "</li>\r\n";
                    } else {
                        $html = $html . "<li>\r\n<a node=\"$id\" >$title</a>\r\n</li>\r\n";
                    }
                }
            }
            $html = $html . "</ul>\r\n";
        }
        return $html;
    }

}
?>