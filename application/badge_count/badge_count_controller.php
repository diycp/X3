<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class badge_count_controller extends base_controller {

	protected function get_data() {

		foreach ($node_list as $val) {
			$badge_function = $val['badge_function'];
			if (!empty($badge_function) and function_exists($badge_function) and ($badge_function != 'badge_sum')) {
				if ($badge_function == 'badge_count_system_folder' or $badge_function == 'badge_count_user_folder') {
					$badge_count[$val['id']] = $badge_function($val['fid']);
				} else {
					$badge_count[$val['id']] = $badge_function();
				}
			}
		};

		//$node_tree = list_to_tree($node_list);
		foreach ($node_list as $key => $val) {
			if ($val['badge_function'] == 'badge_sum') {
				$child_menu = list_to_tree($node_list, $val['id']);
				$child_menu = tree_to_list($child_menu);
				//dump($child_menu);
				$child_menu_id = rotate($child_menu);
				$count = 0;
				if (isset($child_menu_id['id'])) {
					$child_menu_id = $child_menu_id['id'];
					$count = 0;
					foreach ($child_menu_id as $k1 => $v1) {
						if (!empty($badge_count[$v1])) {
							$count += $badge_count[$v1];
						}
					}
				}
				$badge_sum[$val['id']] = $count;
			}
		};

		if (!empty($badge_count)) {
			if (!empty($badge_sum)) {
				$total = $badge_count + $badge_sum;
			} else {
				$total = $badge_count;
			}			
		}
		ajax_return($total);
	}

}
