<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

// 节点模型
use framework\model;

class  node_model extends base_model {
	protected $_validate = array( array('name', 'check_node', '节点已经存在', 0, 'callback'), );

	public function check_node() {
		$map['name'] = $_POST['name'];
		$map['pid'] = isset($_POST['pid']) ? $_POST['pid'] : 0;
		$map['is_del'] = 1;
		if (!empty($_POST['id'])) {
			$map['id'] = array('neq', $_POST['id']);
		}
		$result = $this -> where($map) -> field('id') -> find();
		if ($result) {
			return false;
		} else {
			return true;
		}
	}

	public function access_list($user_id = null) {
		if (empty($user_id)) {
			$user_id = get_user_id();
		}		
		$this -> select("node.badge_function,node.sort, node.id, node.pid, node.name, node.url,sum(role_node.admin) as 'admin',sum(role_node.write) as 'write',sum(role_node.read) as 'read',node.icon");
		$this -> inner('role_node', 'node.id=role_node.node_id');
		$this -> inner('role_user', 'role_user.role_id=role_node.role_id');
		$where[] = array('role_user.user_id', 'eq', $user_id);
		$where[] = array('node.is_del', 'eq', 0);

		$this -> where($where);
		$this -> group('node.id');
		$this -> order('node.sort asc');

		$list = $this -> get_list();
		return $list;
	}

	public function get_top_menu() {
		$user_id = get_user_id();

		$this -> select('distinct *,node.*,node.id, node.pid, node.name, node.url,node.icon');
		$this -> from('node');
		$this -> inner('role_node', 'node.id=role_node.node_id');
		$this -> inner('role_user', 'role_user.role_id=role_node.role_id');

		$where[] = array('role_user.user_id', 'eq', $user_id);
		$where[] = array('node.is_del', 'eq', 0);
		$where[] = array('node.pid', 'eq', 0);

		$this -> where($where);
		$this -> order('node.sort asc');

		$list = $this -> get_list();
		return $list;
	}

}
?>