<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class system_controller extends base_controller {
	//过滤查询字段

	function index() {
		$where_user[] = array('is_del', 'eq', 0);

		$user_count = model('user') -> where($where_user) -> count();
		$this -> assign('user_count', $user_count);

		$where_dept[] = array('is_del', 'eq', 0);

		$dept_count = model('dept') -> where($where_dept) -> count();
		$this -> assign('dept_count', $dept_count);

		$file_count = model('file') -> count();
		$this -> assign('file_count', $file_count);

		$file_space = model('file') -> get_field('sum(size)');

		$this -> assign('file_space', $file_space);

		$where_size[] = array('type', 'eq', 1);

		$file_size = model('system_log') -> where($where_size) -> get_field('time,data', true);

		$file_size = conv_flot($file_size);

		$this -> assign('file_size', $file_size);
		$this -> display();
	}

	function get_flot_data() {
		$range = request('range');
		switch ($range) {
			case 'm' :
				$offset = mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"));
				break;

			case 'q' :
				$offset = mktime(0, 0, 0, date("m") - 3, date("d"), date("Y"));
				break;

			case 'y' :
				$offset = mktime(0, 0, 0, date("m") - 12, date("d"), date("Y"));
				break;

			default :
				break;
		}

		$where_size[] = array('type', 'eq', 1);
		$where_size[] = array('time', 'gt', $offset);

		$system_log_model = new model('system_log');
		$file_size = $system_log_model -> where($where_size) -> get_field('time,data', true);

		$file_size = conv_flot($file_size);

		$where_count[] = array('type', 'eq', 2);
		$where_count[] = array('time', 'gt', $offset);

		$file_count = $system_log_model -> where($where_count) -> get_field('time,data', true);
		$file_count = conv_flot($file_count);

		$return['file_size'] = $file_size;
		$return['file_count'] = $file_count;
		exit(json_encode($return));
	}
}
?>