<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/
use sef\controller;
use sef\model;

class task_controller extends base_controller {
    protected $app_type = 'common';
    protected $auth_map = array('read' => 'select_executor,accept,reject,save_log');

    //过滤查询字段
    function _search_filter(&$map) {
        $map[] = array('is_del', 'eq', '0');
        if (post('keyword')) {
            $map[] = array('name', 'like', post('keyword'));
        }
    }

    public function index() {
        redirect(url('folder', array('fid' => 'all')));
    }

    public function folder() {
        $auth = $this -> auth;
        $this -> assign('auth', $auth);

        $this -> assign('user_id', get_user_id());

        $where = $this -> _search();
        if (method_exists($this, '_search_filter')) {
            $this -> _search_filter($where);
        }

        //$no_finish_task_count = badge_count_no_finish_task();
        //$dept_task_count = badge_count_dept_task();
        //$no_assign_task_count = badge_count_no_assign_task();

        //$this -> assign('no_finish_task_count', $no_finish_task_count);
        //$this -> assign('dept_task_count', $dept_task_count);
        //$this -> assign('no_assign_task_count', $no_assign_task_count);

        $fid = $_GET['fid'];
        $this -> assign("fid", $fid);

        switch ($fid) {
            case 'all' :
                $this -> assign("folder_name", '所有任务');
                if ($auth -> admin) {

                } else {
                    $where[] = array('string', '1=2');
                }
                break;
            case 'sub' :
                $this -> assign("folder_name", '下属任务');

                $dept_id = get_dept_id();
                $sub_dept = get_sub_dept($dept_id);

                $sub_duty_user_list = model('duty') -> get_sub_duty_user_list(get_user_id());

                $sub_position_user = model('position') -> get_sub_position_user_list(get_user_id());
                $user_list = array_intersect($sub_duty_user_list, $sub_position_user);

                if (!empty($user_list)) {
                    $where[] = array('user_id', 'in', $user_list);
                } else {
                    $where[] = array('string', '1=2');
                }
                $where[] = array('dept_id', 'in', $sub_dept);
                break;

            case 'no_finish' :
                $this -> assign("folder_name", '我未完成的任务');

                $where_log[] = array('status', 'lt', 20);
                $where_log[] = array('executor', 'eq', get_user_id());
                $where_log[] = array('type', 'eq', 1);

                $task_list = model("task_log") -> where($where_log) -> get_field('task_id', true);

                if (empty($task_list)) {
                    $where[] = array('string', '1=2');
                } else {
                    $where[] = array('id', 'in', $task_list);
                }

                break;

            case 'finished' :
                $this -> assign("folder_name", '我已完成的任务');

                $where_log[] = array('executor', get_user_id());
                $where_log[] = array('type', 'eq', 1);

                $task_list = model('task_log') -> where($where_log) -> get_field('task_id', true);

                if (empty($task_list)) {
                    $where[] = array('string', '1=2');
                } else {
                    $where[] = array('id', 'in', $task_list);
                    $where[] = array('status', 'eq', 30);
                }
                break;

            case 'my_task' :
                $this -> assign("folder_name", '我发布的任务');
                $where[] = array('user_id', 'eq', get_user_id());
                break;

            case 'my_assign' :
                $this -> assign("folder_name", '我指派的任务');

                $where_log[] = array('assigner', get_user_id());
                $task_list = model('task_log') -> where($where_log) -> get_field('task_id', TRUE);
                if (empty($task_list)) {
                    $where[] = array('string', '1=2');
                } else {
                    $where[] = array('id', 'in', $task_list);
                }
                break;

            default :
                break;
        }

        $model = model('task');

        if (!empty($model)) {
            $list = $this -> _list($model, $where);
        }
        $this -> assign('list', $list);
        $this -> display();
    }

    public function del($id) {
        $this -> _del($id);
    }

    public function read($id) {
        $auth = $this -> auth;
        $this -> assign('auth', $auth);

        $this -> assign('task_id', $id);

        $model = model('task');
        $vo = $model -> find($id);
        $this -> assign('vo', $vo);

        $where_log[] = array('task_id', 'eq', $id);
        $task_log = model('task_log') -> where($where_log) -> get_list();
        $this -> assign('task_log', $task_log);
        if (empty($vo['executor'])) {
            $this -> assign('no_assign', 1);
        }

        $where_accept[] = array('status', 'eq', 0);
        $where_accept[] = array('task_id', 'eq', $id);
        $where_accept[] = array('type', 'eq', 1);
        $where_accept[] = array('executor', 'eq', get_user_id());
        $task_accept = model('task_log') -> where($where_accept) -> find();

        if ($task_accept) {
            $this -> assign('is_accept', 1);
            $this -> assign('task_log_id', $task_accept['id']);
        }

        if ($auth -> admin) {
            $where_dept_accept[] = array('status', 'eq', 0);
            $where_dept_accept[] = array('task_id', 'eq', $id);
            $where_dept_accept[] = array('type', 'eq', 2);
            $where_dept_accept[] = array('executor', 'eq', get_dept_id());
            $task_dept_accept = model('task_log') -> where($where_dept_accept) -> find();
            if ($task_dept_accept) {
                $this -> assign('is_accept', 1);
                $this -> assign('task_log_id', $task_dept_accept['id']);
            }
        }

        $where_working[] = array('status', 'in', '0,10');
        $where_working[] = array('task_id', 'eq', $id);
        $where_working[] = array('type', 'eq', 1);
        $where_working[] = array('executor', 'eq', get_user_id());
        $task_working = model('task_log') -> where($where_working) -> find();

        if ($task_working) {
            $this -> assign('is_working', 1);
            $this -> assign('task_working', $task_working);
        }

        $this -> display();
    }

    public function save_log($id) {

        $model = D("TaskLog");
        if (false === $model -> create()) {
            $this -> error($model -> getError());
        }
        $model -> transactor = get_user_id();
        $model -> transactor_name = get_user_name();

        $status = I('status');
        $finish_rate = I('finish_rate');
        $task_log_id = $id;

        if ($finish_rate == '100.00') {
            $model -> finish_time = to_date(time());
            $model -> status = 20;
            $status = 20;
        }

        if ($model -> status == 22) {
            $model -> finish_time = to_date(time());
        }

        $list = $model -> save();

        $task_id = model('task_log') -> where("id=$task_log_id") -> get_field('task_id');

        if ($status == 10) {
            model('task') -> where("id=$task_id") -> setField('status', 10);
        }

        if ($status >= 20) {
            $where_total_count['task_id'] = array('eq', $task_id);
            $total_count = model('task_log') -> where($where_total_count) -> count();

            $where_finish_count['task_id'] = array('eq', $task_id);
            $where_finish_count['status'] = array('egt', 20);
            $finish_count = model('task_log') -> where($where_finish_count) -> count();

            if ($total_count == $finish_count) {
                model('task') -> where("id=$task_id") -> setField('status', 30);

                $user_id = M('Task') -> where("id=$task_id") -> get_field('user_id');

                $task = model('task') -> where("id=$task_id") -> find();

                $transactor_name = get_user_name();

                $push_data['type'] = '任务';
                $push_data['action'] = '已完成';
                $push_data['title'] = "{$transactor_name}已完成您发起的[{$task['name']}]任务";
                $push_data['content'] = "如有问题，请与[{$transactor_name}]进行沟通。";
                $push_data['url'] = U('Task/read', "id={$task['id']}&return_url&Task/index");

                send_push($push_data, $user_id);
            }
        }

        if ($status == 21) {
            $task_id = I('task_id');
            $forword_executor = I('forword_executor');
            D('Task') -> forword($task_id, $forword_executor);
        }
        if ($status == 22) {

        }
        if ($list !== false) {
            $this -> assign('jumpUrl', get_return_url());
            $this -> success('提交成功!');
            //成功提示
        } else {
            $this -> error('提交失败!');
            //错误提示
        }
    }

    function select_executor() {
        $where_dept[] = array('is_del', 'eq', 0);
        $dept_list = model('dept') -> where($where_dept) -> select('id,pid,name') -> order('sort asc') -> get_list();
        $dept_tree = list_to_tree($dept_list);
        $this -> assign('dept_tree', $dept_tree);

        $where_position[] = array('is_del', 'eq', 0);
        $position_list = model('position') -> where($where_position) -> select('id,name') -> order('sort asc') -> get_list();
        $position_tree = list_to_tree($position_list);
        $this -> assign('position_tree', $position_tree);

        $this -> assign('type', 'dept');

        $this -> display();
    }

    function upload() {
        $this -> _upload();
    }

    function down($attach_id) {
        $this -> _down($attach_id);
    }

}
