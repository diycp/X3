<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  kf_model extends base_model {

    function badge_count($option) {
        switch ($option) {
            case 'baoan' :
                $where[] = array('is_accept', 'eq', 0);
                $where[] = array('type', 'eq', 1);
                return model('kf_talk') -> where($where) -> count();

            case 'jubao' :
                $where[] = array('is_accept', 'eq', 0);
                $where[] = array('type', 'eq', 2);
                return model('kf_talk') -> where($where) -> count();

            case 'todo' :
                $where[] = array('is_accept', 'eq', 0);
                $where[] = array('type', 'eq', 0);
                return model('kf_talk') -> where($where) -> count();

            case 'index' :
                $where[] = array('user_id', 'eq', get_user_id());
                $where[] = array('is_finish', 'eq', 0);
                return model('kf_talk') -> where($where) -> count();

            default :
                return 0;
        }
    }

}
?>