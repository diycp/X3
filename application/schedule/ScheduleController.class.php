<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

namespace Home\Controller;

class ScheduleController extends HomeController {
	protected $config = array('app_type' => 'common', 'read' => 'add_save,index,add,day_view,json,upload,read,search,down,edit,day_view,read2,del');
	//过滤查询字段
	function _search_filter(&$map) {
		if (!empty($_POST["name"])) {
			$map['name'] = array('like', "%" . $_POST['name'] . "%");
		}
		$map['user_id'] = array('eq', get_user_id());
		$map['is_del'] = array('eq', '0');
	}

	public function upload() {
		$this -> _upload();
	}

	function index() {
		$plugin['calendar'] = true;
		$this -> assign("plugin", $plugin);
		$_SESSION['type'] = $_GET['type'];
		$this -> display();
	}

	function json() {
		header("Cache-Control: no-cache, must-revalidate");
		header("Content-Type:text/html; charset=utf-8");
		$user_id = get_user_id();
		$start_date = request('start_date');
		$end_date = request('end_date');
		$type = request('type');

		//实例化日程人员表
		//基本条件
		$where['is_del'] = array('eq', 0);
		$where['start_time'] = array( array('egt', $start_date), array('elt', $end_date));

		//个人：1 部门：2 公司：3：我参与的：4
		switch ($type) {
			case 'private' :
				$where['type'] = array('eq', 3);
				$where['user_id'] = array('eq', get_user_id());
				$list = M('Schedule') -> where($where) -> select();
				exit(json_encode($list));
				break;

			case 'dept' :
				$dept_id = get_dept_id();

				$model = M("Dept");
				$sub_dept = tree_to_list(list_to_tree( M("Dept") -> where('is_del=0') -> select(), $dept_id));
				$sub_dept = rotate($sub_dept, 'id');
				$sub_dept[] = $dept_id;

				$where['is_del'] = array('eq', '0');
				$where['dept_id'] = array('in', $sub_dept);
				$where['type'] = array('eq', 2);
				$list = M('Schedule') -> where($where) -> select();

				exit(json_encode($list));
				break;

			case 'company' :
				$where['type'] = array('eq', 1);
				$list = M('Schedule') -> where($where) -> select();
				exit(json_encode($list));
				break;

			case 'actor' :
				$where1['emp_no'] = array('eq', get_emp_no());
				$s_list = M('ScheduleList') -> where($where1) -> getField('sid', true);

				if (!empty($s_list)) {
					$where['id'] = array('in', $s_list);
				} else {
					$where['_string'] = '1=2';
				}
				$list = M('Schedule') -> where($where) -> select();

				exit(json_encode($list));
				break;

			case 'all' :
				
				//公司日程
				$where1 = $where;
				$where1['type'] = array('eq', 1);				
				$list1 = M('Schedule') -> where($where1) -> getField('id', true);
					
					
				//部门日程
				
				$dept_id = get_dept_id();			
				$model = M("Dept");
				$sub_dept = tree_to_list(list_to_tree( M("Dept") -> where('is_del=0') -> select(), $dept_id));
				$sub_dept = rotate($sub_dept, 'id');
				$sub_dept[] = $dept_id;
				
				$where2 = $where;
				$where2['is_del'] = array('eq', '0');
				$where2['dept_id'] = array('in', $sub_dept);
				$where2['type'] = array('eq', 2);
				$list2 = M('Schedule') -> where($where2) -> getField('id', true);
				
				//个人日程
				$where3 = $where;
				$where3['user_id'] = get_user_id();
				$where3['type'] = array('eq', 3);
				$list3 = M('Schedule') -> where($where3) -> getField('id', true);

				if (empty($list1)) {
					$list1 = array();
				}
				if (empty($list2)) {
					$list2 = array();
				}
				if (empty($list3)) {
					$list3 = array();
				}
				$id_list = array_unique(array_merge($list1, $list2, $list3));

				$where['id'] = array('in', $id_list);
				$list = M('Schedule') -> where($where) -> select();

				exit(json_encode($list));
				break;

			default :
				break;
		}
	}

	function read($id) {
		$plugin['jquery-ui'] = true;
		$this -> assign("plugin", $plugin);

		$model = M('Schedule');
		$list = $_REQUEST['list'];
		$this -> assign("list", $list);
		$list = array_filter(explode("|", $list));
		$current = array_search($id, $list);

		if ($current !== false) {
			$next = $list[$current + 1];
			$prev = $list[$current - 1];
		}
		$this -> assign('next', $next);
		$this -> assign('prev', $prev);

		$where['id'] = $id;
		//$where['user_id'] = get_user_id();
		$vo = $model -> where($where) -> find();
		$auth = $this -> auth;
		$this -> assign('auth', $auth);
		$this -> assign('vo', $vo);
		$this -> display();
	}

	function search() {

		$plugin['date'] = true;
		$this -> assign("plugin", $plugin);

		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		if (empty($_POST["be_start_date"]) && empty($_POST["en_start_date"])) {
			$start_date = to_date(mktime(0, 0, 0, date("m"), 1, date("Y")), 'Y-m-d');
			$end_date = to_date(mktime(0, 0, 0, date("m") + 1, 0, date("Y")), 'Y-m-d');
			$map['start_time'] = array( array("egt", $start_date), array("elt", $end_date));
		} else {
			$start_date = $_POST["be_start_date"];
			$end_date = $_POST["en_start_date"];
		}

		$this -> assign('start_date', $start_date);
		$this -> assign('end_date', $end_date);

		$model = model("Schedule");
		if (!empty($model)) {
			$this -> _list($model, $map);
		}
		$this -> assign('type_data', $this -> type_data);
		$this -> display();
		return;
	}

	public function down($attach_id) {
		$this -> _down($attach_id);
	}

	public function add() {
		$plugin['jquery-ui'] = true;
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);

		$auth = $this -> auth;
		$this -> assign("auth", $auth);
		$this -> display();
	}

	public function add_save() {
		$model = M('Schedule');
		$push = M('push');

		$schedule_list = M('schedule_list');
		$list = $model -> create();
		$model -> user_id = get_user_id();
		$model -> dept_id = get_dept_id();

		$push_data['type'] = "日程";
		$push_data['action'] = '需要查阅';
		$push_data['title'] = $list['name'];
		$push_data['content'] = '发送人：' . get_dept_name() . "-" . get_user_name();
		$push_data['url'] = U("Sign/index");

		$data['data'] = json_encode($push_data, JSON_UNESCAPED_UNICODE);
		//将数据转成JSON 存到PUSH表里面
		$data['user_id'] = get_user_id();
		$data['status'] = 3;
		$start_time = request('start_time');
		$data['time'] = strtotime("$start_time -30 minute");
		$push -> add($data);

		$rs = $model -> add();

		$a = array_filter(explode(";", request('actor')));
		if ($rs) {
			foreach ($a as $k) {
				$str = substr($k, strpos($k, "|"));
				$user_id = str_replace('|', '', $str);
				$data['sid'] = $rs;
				$data['user_id'] = $user_id;
				$data['start_time'] = date("Y-m-d H:i:s", time());
				$schedule_list -> add($data);
			}
			$this -> success('操作成功', U('index'), 1);
		}
	}

	public function edit($id) {
		$plugin['jquery-ui'] = true;
		$plugin['date'] = true;
		$plugin['uploader'] = true;
		$plugin['editor'] = true;
		$this -> assign("plugin", $plugin);

		$id = request('id');
		$model = M('Schedule');
		$where['user_id'] = get_user_id();
		$where['id'] = $id;
		$vo = $model -> where($where) -> find();
		$auth = $this -> auth;
		$this -> assign('auth', $auth);
		$this -> assign('vo', $vo);
		$this -> display();
	}

	public function day_view() {
		$this -> index();
	}

	public function read2($id) {
		$plugin['jquery-ui'] = true;
		$this -> assign("plugin", $plugin);
		$this -> read($id);
	}

	public function del($id) {
		$this -> _del($id);
	}

}
?>