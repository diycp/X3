<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

use sef\controller;
use sef\model;

class system_folder_controller extends base_controller {
	protected $app_type = 'widget';
	//过滤查询字段
	function _search_filter(&$map) {
		$map['name'] = array('like', "%" . $_POST['name'] . "%");
		$map['is_del'] = array('eq', '0');
	}

	function add() {
		$controller = request('controller');
		$this -> assign('controller', $controller);
		$this -> assign('has_pid', request('has_pid'));
		$this -> display();
	}

	function index() {
		$model = new system_folder_model();
		if (IS_POST) {
			$opmode = $_POST["opmode"];
			if (false === $model -> create()) {
				$this -> error($model -> getError());
			}
			if ($opmode == "add") {
				$model -> controller = APP_NAME;
				$list = $model -> add();
				if ($list != false) {
					$this -> success("添加成功");
				} else {
					$this -> error("添加成功");
				}
			}
			if ($opmode == "edit") {
				$list = $model -> save();
				if ($list != false) {
					$this -> success("保存成功");
				} else {
					$this -> error("保存失败");
				}
			}
			if ($opmode == "del") {
				$this -> _del($model -> id);
			}
		}
		$folder_list = $model -> get_folder_list();
		$tree = list_to_tree($folder_list);

		$this -> assign('tree', $tree);
		$this -> assign("folder_list", $folder_list);
		$this -> assign('controller', APP_NAME);
		$this -> display('index');
	}

	function read($id) {
		$system_folder_model = new model('system_folder');
		$where[] = array('id', 'eq', $id);
		$data = $system_folder_model -> where($where) -> find($id);
		if ($data !== false) {// 读取成功
			$return['data'] = $data;
			exit(json_encode($return));
		}
	}

	function del($id, $name = APP_NAME, $return_flag = false) {
		$model = new system_folder_model();

		$vo = $model -> find($id);
		$controller = $vo['controller'];

		$model_count = new model($controller);
		$where_count[] = array('folder', 'eq', $id);
		$where_count[] = array('is_del', 'eq', 0);
		$count = $model_count -> where($where_count) -> count();

		$sub_folder_list = tree_to_list(list_to_tree($model -> get_folder_list(), $id));
		if ($count > 0 || !empty($sub_folder_list)) {// 读取成功
			$this -> error('只能删除空文件夹');
		} else {
			$data['id'] = $id;
			$data['is_del'] = 1;
			$result = $model -> save($data);
			if ($return_flag) {
				return $result;
			}
			if ($result) {
				$this -> success('删除成功');
			}
		}
	}

	function select_pid($controller) {
		$where[] = array('controller', 'eq', $controller);
		$where[] = array('is_del', 'eq', 0);

		$dept_model = new model('system_folder');
		$menu = $dept_model -> where($where) -> select('id,pid,name') -> order('sort asc') -> get_list();

		$tree = list_to_tree($menu);
		$this -> assign('tree', $tree);
		$root['id'] = 0;
		$root['name'] = '根节点';
		$root['child'] = $tree;
		$pid = array();
		$this -> assign('pid', $pid);
		$this -> display();
	}

}
