<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/

use sef\model;

class  group_model extends base_model {

	function get_group_list($user_id) {
		$table = $this -> tablePrefix . 'group_user';
		$rs = $this -> db -> query('select a.group_id from ' . $table . ' as a where a.user_id=' . $user_id . ' ');
		return $rs;
	}

	function get_user_list($group_id) {
		$where['group_id'] = array('eq', $group_id);
		$rs = M("GroupUser") -> where($where) -> getField('user_id', true);
		return $rs;
	}

	function del_user($group_id, $user_list) {
		if (empty($user_list)) {
			return true;
		}
		if (is_array($user_list)) {
			$user_list = array_filter($user_list);
		} else {
			$user_list = explode(",", $user_list);
			$user_list = array_filter($user_list);
		}
		$user_list = implode(",", $user_list);

		$table = $this -> tablePrefix . 'group_user';

		$sql = 'delete from ' . $table . ' where user_id in (' . $user_list . ') and group_id=\'' . $group_id . '\'';

		$result = $this -> db -> execute($sql);

		if ($result === false) {
			return false;
		} else {
			return true;
		}
	}

	function save_user($group_id, $user_list) {
		if (empty($group_id)) {
			return true;
		}
		if (empty($user_list)) {
			return true;
		}

		if (is_array($user_list)) {
			$user_list = array_filter($user_list);
		} else {
			$user_list = explode(",", $user_list);
			$user_list = array_filter($user_list);
		}
		$data['group_id'] = $group_id;
		foreach ($user_list as $user) {
			$data['user_id'] = $user;
			model('group_user') -> add($data);
		}

		return true;
	}

}
?>