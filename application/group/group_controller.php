<?php
/*--------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐

 Copyright (c) 2013 http://www.smeoa.com All rights reserved.

 Author:  jinzhu.yin<smeoa@qq.com>

 Support: https://git.oschina.net/smeoa/xiaowei
 --------------------------------------------------------------*/

// 用户组模块
use sef\controller;
use sef\model;

class group_controller extends base_controller {

	//过滤查询字段
	function _search_filter(&$map) {
		$map[] = array('is_del', 'eq', '0');
		$keyword = request('keyword');
		if (!empty($keyword)) {
			$map['User.name|emp_no|Position.name|Dept.name'] = array('like', "%" . $keyword . "%");
		}
	}

	public function index() {
		$list = model('group') -> select('id,name,is_public,is_del') -> order('sort asc') -> get_list();
		$this -> assign('list', $list);
		$this -> display();
	}

	public function del($id) {
		$where_group[] = array('id', 'eq', $id);
		model('group') -> where($where_group) -> delete();

		$where_group_user[] = array('group_id', 'eq', $id);
		model('group_user') -> where($where_group_user) -> delete();
		$this -> success('删除成功');
	}

	public function get_node_list() {
		$role_id = $_POST["role_id"];
		$model = model("Role");
		$data = $model -> get_node_list($role_id);
		if ($data !== false) {// 读取成功
			$return['data'] = $data;
			$return['status'] = 1;
			$this -> ajaxReturn($return);
		}
	}

	public function user($id) {
		$map = $this -> _search();
		if (method_exists($this, '_search_filter')) {
			$this -> _search_filter($map);
		}

		$row_info = model('group') -> find($id);		
		$this -> assign('row_info', $row_info);

		$where_group_user[] = array('group_id', 'eq', $id);
		$group_user = model('group_user') -> where($where_group_user) -> get_field('user_id', true);

		if (!empty($group_user)) {
			$where_user_list[] = array('user.id', 'in', $group_user);
		} else {
			$where_user_list[] = array('string', '1=2');
		}

		$model = model("user");
		$model -> left('dept', 'dept.id=user.dept_id');
		$model -> left('position', 'position.id=user.position_id');
		$model -> select('user.*,dept.name dept_name,position.name position_name');
		$user_list = $model -> where($where_user_list) -> get_list();
		$this -> assign("user_list", $user_list);

		$this -> display();
	}

	public function add_user($group_id) {
		$this -> assign('group_id', $group_id);

		$where_user[] = array('group_id', 'eq', $group_id);
		$user_list = model('group_user') -> where($where_user) -> get_field('user_id', true);
		$map[] = array('user.is_del', 'eq', 0);
		if (!empty($user_list)) {
			$map[] = array('id', 'not in', $user_list);
		}
		$model = model("user");
		$model -> left('dept', 'dept.id=user.dept_id');
		$model -> left('position', 'position.id=user.position_id');
		$model -> select('user.*,dept.name dept_name,position.name position_name');
		$user_list = $model -> where($map) -> get_list();
		$this -> assign("user_list", $user_list);
		$this -> display();
	}

	public function del_user($group_id, $user_id) {
		$model = model("Group");
		$result = $model -> del_user($group_id, $user_id);
		if ($result === false) {
			$this -> error('操作失败！');
		} else {
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('操作成功！');
		}
	}

	public function save_user($group_id, $id) {		
		$result = model('group') -> save_user($group_id, $id);		
		if ($result === false) {
			$this -> error('操作失败！');
		} else {
			$this -> assign('jumpUrl', get_return_url());
			$this -> success('操作成功！');
		}
	}

}
?>