<?php
use sef\controller;
use sef\model;

class page_header_controller extends controller {
	protected $app_type = 'widget';

	public function simple($name) {
		$this -> assign('name', $name);
		echo $this -> fetch('simple');
	}

	public function search($name) {
		$this -> assign('name', $name);
		echo $this -> fetch('search');
	}

	public function adv_search($name) {
		$this -> assign('name', $name);
		echo $this -> fetch('adv_search');
	}

	public function popup($name) {
		$this -> assign('name', $name);
		echo $this -> fetch('popup');
	}
	
	public function sub($name) {
		$this -> assign('name', $name);
		echo $this -> fetch('sub');
	}
}
?>