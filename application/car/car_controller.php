<?php

use sef\controller;
use sef\model;

class car_controller extends controller {

    public function index() {
        echo encrypt(18);
        $qrcode = request('qrcode');
     
        $id = decrypt($qrcode);

        $this -> assign('qrcode', $qrcode);

        $where[] = array('id', 'eq', $id);
        $ret = model('car') -> where($where) -> find();

        if ($ret != false) {
            $this -> assign('vo', $ret);
        }

        if (strlen($qrcode) != 34) {
            //echo encrypt(6);
        }
        $this -> display();
    }

    public function chezhu() {
        $qrcode = request('qrcode');
        $this -> assign('qrcode', $qrcode);
        $id = decrypt($qrcode);

        $where[] = array('id', 'eq', $id);
        $ret = model('car') -> where($where) -> find();
        if ($ret !== false) {
            $this -> assign('opmode', 'edit');
            $this -> assign('vo', $ret);
            $check_auth = session('check_auth');
        } else {
            $this -> assign('opmode', 'add');
            $check_auth = true;
        }

        $this -> assign('check_auth', $check_auth);
        $this -> display();
    }

    public function nuoche() {
        $qrcode = request('qrcode');
        $id = decrypt($qrcode);

        $where[] = array('id', 'eq', $id);

        $ret = model('car') -> where($where) -> find();

        if ($ret != false) {
            $this -> assign('vo', $ret);
        }
        $this -> display();
    }

    public function about() {
        $this -> display();
    }

    public function qrcode($id) {
        vendor();
        $qrcode = encrypt($id);
        $qrCode = new Endroid\QrCode\QrCode();
        $str = "http://car.smeoa.com/index.php?app=car&qrcode=" . $qrcode;
        //echo $str;
        $qrCode -> setText($str);
        $qrCode -> setSize(600);
        $qrCode -> setPadding(10);
        $qrCode -> render();
        die ;
    }

    public function build() {
        $id = request('id');
        $this -> assign('id', $id);
        $this -> display();
    }

    /** 保存操作  **/
    function save() {
        $this -> _save();
    }

    protected function _save($name = APP_NAME) {
        $opmode = request('opmode');
        switch($opmode) {
            case "add" :
                $this -> _insert($name);
                break;

            case "edit" :
                $this -> _update($name);
                break;

            case "check" :
                $this -> _check_pwd();
                break;
            default :
                $this -> error("非法操作");
        }
    }

    protected function _check_pwd() {
        $qrcode = post('qrcode');
        $pwd = post('pwd');

        $where[] = array('id', 'eq', decrypt($qrcode));
        $where[] = array('pwd', 'eq', $pwd);
        //print_r($where);

        $ret = model('car') -> where($where) -> find();
        if ($ret !== false) {
            session('check_auth', true);
            $return['status'] = 1;
            $return['info'] = '登录成功';
        } else {
            $return['status'] = 0;
            $return['info'] = '密码错误';
        }
        ajax_return($return);
    }

    /** 插入新新数据  **/
    protected function _insert($name = APP_NAME) {
        $model = model($name);
        if (false === $model -> create()) {
            $this -> error($model -> getError());
        }
        $qrcode = post('qrcode');
        $model -> id = decrypt($qrcode);
        $list = $model -> add();
        if ($list !== false) {
            $this -> success('新增成功!');
        } else {
            $this -> error('新增失败!');
        }
    }

    /* 更新数据  */
    protected function _update($name = APP_NAME) {
        $model = model($name);
        if (false === $model -> create()) {
            $this -> error($model -> getError());
        }
        $qrcode = request('qrcode');
        $model -> id = decrypt($qrcode);
        $list = $model -> save();
        if (false !== $list) {
            $this -> success('编辑成功!', get_return_url());
        } else {
            $this -> error('编辑失败!');
        }
    }

}
