<?php
if (version_compare(PHP_VERSION, '5.4', '<')) {
	die(':( require PHP > 5.4 !');
}

// 是否调试模式
define('DEBUG_MODE', true);
define('DS', DIRECTORY_SEPARATOR);

// 定义运行程序目录
define('APP_PATH', realpath('../application') . DS);
define('FRAMEWORK_PATH', realpath('../framework') . DS);
define('WWW_PATH', realpath('../www') . DS);

include FRAMEWORK_PATH . 'sef.php';
?>