layui.define(function(exports) {
	jQuery = layui.jquery;
	load_js('layui/plugins/toastr/toastr.min.js', function() {
		var obj = {
			push_info : function($msg) {
				var position;
				if ($msg.action.length) {
					$title = '<h3>[' + $msg.type + '] [' + $msg.action + ']</h3>';
				} else {
					$title = '<h3>[' + $msg.type + ']</h3>';
				}
				$content = '<b>' + $msg.title + '</b><br>' + $msg.content;

				if (is_mobile()) {
					position = "toast-top-full-width";
				} else {
					position = "toast-bottom-right";
				}
				toastr.options = {
					"closeButton" : true,
					"positionClass" : position,
					"timeOut" : ws_push_time * 1000
				};
				toastr.info($content, $title);
			}
		};
		win_exp(obj);
	});
	exports('toastr', {});
});

