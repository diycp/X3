layui.define(['layer', 'toastr'], function(exports) {
	var $ = layui.jquery;
	window.$ = layui.jquery;
	var layer = layui.layer;
	var obj = {
		winopen : function(url, w, h) {
			url = fix_url(url);
			var index = layer.open({
				type : 2,
				title : false,
				area : [w + 'px', h + 'px'],
				shade : 0.6,
				closeBtn : 0,
				shadeClose : false,
				scrollbar : false,
				content : [url, 'no']
			});
			if (is_mobile()) {
				layer.full(index);
			}
		},
		send_form : function(from_id, post_url, return_url, callback) {
			check_form(from_id, function(ret) {
				if (ret.status) {
					if ($("#ajax").val() == 1) {
						var vars = $("#" + from_id).serialize();
						$.ajax({
							type : "POST",
							url : post_url,
							data : vars,
							dataType : "json",
							success : function(data) {
								callback(data);
							}
						});
					} else {
						//取消beforeunload事件
						$(window).unbind('beforeunload', null);
						$("#" + from_id).attr("action", post_url);
						if (return_url) {
							set_return_url(return_url);
						}
						$("#" + from_id).submit();
					}
				} else {
					console.log(ret);
					layer.msg(ret.info);
					ret.dom.focus();
					return false;
				}
			});
		},
		/* ajax提交*/
		send_ajax : function(url, vars, callback) {
			return $.ajax({
				type : "POST",
				url : url,
				data : vars + "&ajax=1",
				dataType : "json",
				success : callback
			});
		},
		push_info : function($msg) {
			var position;
			if ($msg.action.length) {
				$title = '<h3>[' + $msg.type + '] [' + $msg.action + ']</h3>';
			} else {
				$title = '<h3>[' + $msg.type + ']</h3>';
			}
			$content = '<b>' + $msg.title + '</b><br>' + $msg.content;

			if (is_mobile()) {
				position = "toast-top-full-width";
			} else {
				position = "toast-bottom-right";
			}
			toastr.options = {
				"closeButton" : true,
				"positionClass" : position,
				"timeOut" : ws_push_time * 1000000
			};
			toastr.info($content, $title);
		},
		winprint : function() {
			setTimeout(function() {
				window.print();
			}, 300);
		}
	};
	win_exp(obj);
	exports('x', obj);
});

