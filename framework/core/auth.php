<?php
namespace sef;
class auth {
	public $admin = false;
	public $write = false;
	public $read = false;
	public $status = false;
	public $info = '';

	public function __construct($app_type = null, $auth_map = null) {
		if (empty($app_type)) {
			$this -> status = true;
			return;
		}

		switch($app_type) {
			case 'widget' :
				$this -> admin = true;
				$this -> write = true;
				$this -> read = true;
				$this -> status = true;
				return;
				break;

			case 'system' :
				$this -> status = true;
				return;
				break;

			case 'admin' :
				$this -> get_auth();
				if ($this -> admin) {
					$this -> status = true;
					return;
				} else {
					$this -> status = false;
					$this -> info = '必须是管理员权限才能操作';
				}
				break;

			case 'folder' :
				if (in_array(METHOD_NAME, array('folder_manage', 'field_manage'))) {
					$this -> get_auth();                    
					break;
				}
				if (isset($_REQUEST['fid'])) {
					$fid = $_REQUEST['fid'];
					$system_folder_model = model('system_folder');
					$folder_auth = $system_folder_model -> get_folder_auth($fid);
					$this -> admin = $folder_auth -> admin;
					$this -> write = $folder_auth -> write;
					$this -> read = $folder_auth -> read;
					break;
				}

				if (isset($_REQUEST['id'])) {
					$id = $_REQUEST['id'];
					if (!empty($id)) {
						if (is_array($id)) {
							$where[] = array('id', "in", array_filter($id));
						} else {
							$where[] = array('id', 'in', array_filter(explode(',', $id)));
						}
						$model = model(APP_NAME);
						$folder_id = $model -> where($where) -> get_field('folder');

						$system_folder_model = model('system_folder');
						$folder_auth = $system_folder_model -> get_folder_auth($folder_id);

						$this -> admin = $folder_auth -> admin;
						$this -> write = $folder_auth -> write;
						$this -> read = $folder_auth -> read;
						break;
					}
				}
				$this -> get_auth();
				break;
			default :
				$this -> get_auth();
				break;
		}

		$result = false;
 
		if (isset($auth_map['admin'])) {
			$app_auth_admin = array_filter(explode(',', $auth_map['admin']));
			if (!$result and in_array(METHOD_NAME, $app_auth_admin)) {
				$this -> status = $this -> admin;
				$result = true;
			}
		}

		if (isset($auth_map['write'])) {
			$app_auth_write = array_filter(explode(',', $auth_map['write']));
			if (!$result and in_array(METHOD_NAME, $app_auth_write)) {
				$this -> status = $this -> write;
				$result = true;
			}
		}

		if (isset($auth_map['read'])) {
			$app_auth_read = array_filter(explode(',', $auth_map['read']));
			if (!$result and in_array(METHOD_NAME, $app_auth_read)) {
				$this -> status = $this -> read;
				$result = true;
			}
		}

		if (isset($auth_map['public'])) {
			$app_auth_public = array_filter(explode(',', $auth_map['public']));
			if (!$result and in_array(METHOD_NAME, $app_auth_public)) {
				$this -> status = true;
				$result = true;
			}
		}


		$default_auth_admin = array_filter(explode(',', config('auth_admin')));
		$default_auth_write = array_filter(explode(',', config('auth_write')));
		$default_auth_read = array_filter(explode(',', config('auth_read')));

		if (!$result and in_array(METHOD_NAME, $default_auth_admin)) {
			$this -> status = $this -> admin;
			$result = true;
		}

		if (!$result and in_array(METHOD_NAME, $default_auth_write)) {
			$this -> status = $this -> write;
			$result = true;
		}

		if (!$result and in_array(METHOD_NAME, $default_auth_read)) {
			$this -> status = $this -> read;
			$result = true;
		}
	}

	private function get_auth($inherit = true) {

		$nodel_model = new model('node');
		$user_id = get_user_id();

		$nodel_model -> select("node.pid,node.url,sum(role_node.admin) as 'admin',sum(role_node.write) as 'write',sum(role_node.read) as 'read'");
		$nodel_model -> inner('role_node', 'node.id=role_node.node_id');
		$nodel_model -> inner('role_user', 'role_user.role_id=role_node.role_id');

		$where[] = array('role_user.user_id', 'eq', $user_id);
		$where[] = array('node.is_del', 'eq', 0);

		$nodel_model -> where($where);
		$nodel_model -> group('node.id');
		$nodel_model -> order('node.sort asc');

		$access_list = $nodel_model -> get_list();
		$access_list = array_filter($access_list, array($this, 'filter_app'));

		$access_list = rotate($access_list);
		$app_list = $access_list['url'];

		$app_list = array_map(array($this, "get_app"), $app_list);

		$access_list_admin = array_filter(array_combine($app_list, $access_list['admin']));
		$access_list_write = array_filter(array_combine($app_list, $access_list['write']));
		$access_list_read = array_filter(array_combine($app_list, $access_list['read']));

		$app_name = strtolower(APP_NAME);

		$this -> admin = array_key_exists($app_name, $access_list_admin) || array_key_exists("##" . $app_name, $access_list_admin);

		if ($this -> admin == true and $inherit) {
			$this -> write = true;
			$this -> read = true;
			return;
		}

		$this -> write = array_key_exists($app_name, $access_list_write) || array_key_exists("##" . $app_name, $access_list_write);
		if ($this -> write == true and $inherit) {
			$this -> read = true;
			return;
		}

		$this -> read = array_key_exists($app_name, $access_list_read) || array_key_exists("##" . $app_name, $access_list_read);

		return;
	}

	private function get_app($str) {
		$arr_str = explode("/", $str);
		return strtolower($arr_str[0]);
	}

	private function filter_app($str) {
		if (empty($str['admin']) && empty($str['write']) && empty($str['read'])) {
			return false;
		}
		if (strpos($str['url'], 'index')) {
			return true;
		}
		return false;
	}

}
