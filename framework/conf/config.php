<?php

$config -> db_host = '127.0.0.1';
$config -> db_name = 'sef';
$config -> db_user = 'root';
$config -> db_password = '';
$config -> table_prefix = 'think_';

$config -> user_auth_key = 'auth_id';
$config -> user_auth_gateway = 'public/login';

$config -> cookie_prefix = 'xiaowei';
$config -> auth_admin = 'restore,destory,import,export';
$config -> auth_write = 'add,edit,upload,del_file,save,del';
$config -> auth_read = 'index,read,down,folder,export';
$config -> avatar_path = 'uploads/avatar/';
$config -> chunk_upload = true;
$config -> tpl_path = './uploads/templete';
