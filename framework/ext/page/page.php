<?php
/*---------------------------------------------------------------------------
 小微OA系统 - 让工作更轻松快乐
 Copyright (c) 2013 http://www.smeoa.com All rights reserved.
 Author:  jinzhu.yin<smeoa@qq.com>
 Support: https://git.oschina.net/smeoa/xiaowei
 -------------------------------------------------------------------------*/
class page {
	// 默认列表每页显示行数
	public $list_rows;
	protected $total_rows;
	// 分页总页面数
	protected $total_pages;
	// 当前页数
	protected $current_page;
	public $first_row;
	/**
	 * 架构函数
	 * @access public
	 * @param array $total  总的记录数
	 * @param array $row_count  每页显示记录数
	 * @param array $vars  分页跳转的参数
	 */
	public function __construct($total_rows, $list_rows = '') {
		$this -> total_rows = $total_rows;
		if (!empty($list_rows)) {
			$this -> list_rows = intval($list_rows);
		}
		$this -> total_pages = ceil($this -> total_rows / $this -> list_rows);
		$this -> current_page = !empty($_POST['p']) ? intval($_POST['p']) : 1;
		if ($this -> current_page < 1) {
			$this -> current_page = 1;
		} elseif (!empty($this -> total_pages) && $this -> current_page > $this -> total_pages) {
			$this -> current_page = $this -> total_pages;
		}
		$this -> first_row = $this -> list_rows * ($this -> current_page - 1);
	}

	/**
	 * 分页显示输出
	 * @access public
	 */
	public function show() {
		$pre_page = $this -> current_page - 1;
		$next_page = $this -> current_page + 1;

		$html[] = '<div class="pagination">';
		$html[] = '					<form id="form_pagination" method="post" style="width:100%;">';
		$html[] = '						<div class="row" style="justify-content:flex-end;">';
		$html[] = '							<div class="box">';
		$html[] = '								<label class="label">每页</label>';
		$html[] = '								<div class="input-box" style="width:45px;">';
		$html[] = '									<input name="list_rows" class="input text-center" value="' . $this -> list_rows . '" oninput="submit_pagination(this);" onkeyup="submit_pagination(this);" onfocus="this.value=\'\'" onblur="if(this.value==\'\')this.value=' . $this -> list_rows . '">';
		$html[] = '								</div>';
		$html[] = '								<label class="label">条</label>';
		$html[] = '							</div>';
		$html[] = '							<div class="box">';
		$html[] = '								<button class="btn" onclick="this.form.p.value=' . $pre_page . ';this.form.submit();"><i class="fa fa-angle-left" ></i></button>';
		$html[] = '								<div class="input-box" style="width:45px;padding-left:7px;padding-right:0px;">';
		$html[] = '									<input name="p"  class="input text-center"  value="' . $this -> current_page . '" oninput="submit_pagination(this);"   onkeyup="submit_pagination(this);"  onfocus="this.value=\'\'" onblur="if(this.value==\'\')this.value=' . $this -> current_page . '">';
		$html[] = '								</div>';
		$html[] = '								<label class="label">/ ' . $this -> total_pages . '</label>';
		$html[] = '								<button class="btn" onclick="this.form.p.value=' . $next_page . ';"><i class="fa fa-angle-right" ></i></button>';
		$html[] = '							</div>';
		$html[] = '						</div>';
		$html[] = '					</form>';
		$html[] = '				</div>';
		return implode('', $html);
	}
}
